import {KidorahPage} from "./app.po";

describe('kidorah App', function () {
  let page: KidorahPage;

  beforeEach(() => {
    page = new KidorahPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('kd works!');
  });
});
