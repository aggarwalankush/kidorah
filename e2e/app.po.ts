export class KidorahPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('kd-root h1')).getText();
  }
}
