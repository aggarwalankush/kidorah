import {Component} from "@angular/core";

@Component({
  selector: 'kd-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent {
}
