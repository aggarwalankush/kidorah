import {BrowserModule} from "@angular/platform-browser";
import {NgModule, NgModuleFactoryLoader} from "@angular/core";
import {AppComponent} from "./app.component";
import {routing} from "./app.routing";
import {SharedModule} from "./shared";
import {AsyncNgModuleLoader} from "./faltu-loader";
import {LoginRegisterModule} from "./login-register";
import {HomeModule} from "./home";

@NgModule({
  imports: [
    BrowserModule,
    SharedModule.forRoot(),
    LoginRegisterModule,
    HomeModule,
    routing
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    {provide: NgModuleFactoryLoader, useClass: AsyncNgModuleLoader}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
