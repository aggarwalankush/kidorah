import {RouterModule, Routes} from "@angular/router";
import {load} from "./faltu-loader";

const routes: Routes = [
  {
    path: 'reset/:token',
    loadChildren: load(() => new Promise(resolve => {
      (require as any).ensure([], require => {
        resolve(require('./reset/reset.module').default);
      })
    }))
  }, {
    path: '**', redirectTo: '', pathMatch: 'full'
  }
];

export const routing = RouterModule.forRoot(routes);
