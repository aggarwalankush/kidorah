import {NgModule} from "@angular/core";
import {homeRouting} from "./home.routing";
import {HomeComponent} from "./home.component";
import {FooterComponent, HeaderComponent} from "../layouts";
import {LandingComponent, SiteadminLandingComponent} from "../landing";
import {SharedModule} from "../shared";

@NgModule({
  imports: [
    SharedModule,
    homeRouting
  ],
  declarations: [
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    LandingComponent,
    SiteadminLandingComponent
  ]
})
export class HomeModule {
}
