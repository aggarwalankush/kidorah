import {Routes, RouterModule} from "@angular/router";
import {HomeComponent} from "./home.component";
import {LandingComponent, SiteadminLandingComponent} from "../landing";
import {AuthGuardService, UserGuardService, SiteAdminGuardService} from "../shared";

export const homeRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        component: LandingComponent,
        canActivate: [UserGuardService]
      },
      {
        path: 'sa',
        component: SiteadminLandingComponent,
        canActivate: [SiteAdminGuardService]
      }
    ]
  }
];

export const homeRouting = RouterModule.forChild(homeRoutes);
