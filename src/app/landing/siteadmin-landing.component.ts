import {Component, OnInit, OnDestroy} from "@angular/core";
import * as _ from "lodash";
import {HttpService, ErrorService, NotifyService, TableService} from "../shared";

@Component({
  templateUrl: 'siteadmin-landing.component.html'
})
export class SiteadminLandingComponent implements OnInit, OnDestroy {

  constructor(private httpService: HttpService,
              private tableService: TableService,
              private notifyService: NotifyService,
              private errorService: ErrorService) {
  }

  ngOnInit(): void {
    this.tableService.initTable('newTeamTable', {
      ajax: this.newTablePagination.bind(this)
    });
    this.tableService.initTable('approvedTeamTable', {
      ajax: this.approvedTablePagination.bind(this)
    });
    this.tableService.initTable('rejectedTeamTable', {
      ajax: this.rejectedTablePagination.bind(this)
    });
    this.tableService.createEvent('teamEditEvents',
      'click #approveBtn, #rejectBtn', this.approveRejectTeam.bind(this));
  }

  ngOnDestroy(): void {
    this.tableService.destroyTable([
      'newTeamTable', 'approvedTeamTable', 'rejectedTeamTable'
    ]);
    this.tableService.removeEvent('teamEditEvents');
  }

  newTablePagination(params) {
    this.serverPagination(params);
  }

  approvedTablePagination(params) {
    params.data.status = 'approved';
    this.serverPagination(params);
  }

  rejectedTablePagination(params) {
    params.data.status = 'rejected';
    this.serverPagination(params);
  }

  serverPagination(params) {
    let self = this;
    this.httpService.get('/team/all', params.data)
      .subscribe(
        (data: TeamResponse) => {
          let rows = _.map(data.teams, team=> {
            let row = {};
            let team_name = `${team.team_name}<br/><small>Created ${team.created}</small>`;
            _.set(row, 'team_name', team_name);
            _.set(row, 'edit', self.getEditColumn(params.data.status));
            return _.merge({}, team, row);
          });
          params.success({
            total: data.total,
            rows: rows
          });
        },
        error => self.errorService.handleError(error)
      );
  }

  getEditColumn(status) {
    let approveBtn = `<a class="btn btn-success col-md-5" id="approveBtn">
                              <i class="fa fa-check"></i> Approve </a>`;
    let rejectBtn = `<a class="btn btn-danger col-md-5" id="rejectBtn">
                             <i class="fa fa-ban"></i> Reject </a>`;
    let editStr = '';
    if (status && status === 'approved') {
      editStr = rejectBtn;
    } else if (status && status === 'rejected') {
      editStr = approveBtn;
    } else {
      editStr = approveBtn + rejectBtn;
    }
    editStr = `<div class="row">${editStr}</div>`;
    return editStr;
  }

  approveRejectTeam(e, value, row) {
    let self = this;
    let status = '';
    if (e.target.id === 'approveBtn') {
      status = 'approved';
    } else if (e.target.id === 'rejectBtn') {
      status = 'rejected';
    }
    this.httpService.patch('/team/' + row.id, {status: status})
      .subscribe(data=> {
          if (data['success'] === true) {
            self.notifyService.notify(row.team_name.split('<')[0] + ' ' + status);
            self.tableService.refreshTable([
              'newTeamTable', 'approvedTeamTable', 'rejectedTeamTable'
            ]);
          }
        },
        error => self.errorService.handleError(error)
      );
  }
}

interface TeamResponse {
  total: number,
  teams: Array<{
    id: number,
    created: string,
    team_name: string,
    team_admin: string,
    team_member: string,
    product_allowance: number
  }>
}
