import {Component} from "@angular/core";

@Component({
  selector: 'kd-footer',
  template: `
    <footer>
      <div class="pull-right">
          <i class="fa fa-group"></i><b> Kidorah </b>
          <i class="fa fa-copyright"></i>
          2016 All Rights Reserved. Privacy and Terms
      </div>
      <div class="clearfix"></div>
    </footer>
  `
})
export class FooterComponent {
  constructor() {
  }
}
