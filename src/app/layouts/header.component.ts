import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "../shared";

@Component({
  selector: 'kd-header',
  templateUrl: 'header.component.html'
})
export class HeaderComponent implements OnInit {
  name: string = '';
  isSiteAdmin: boolean = false;

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.name = this.authService.getUserName();
    this.isSiteAdmin = this.authService.isSiteAdmin();
  }

  logout() {
    $('[data-toggle="tooltip"]').tooltip('destroy');
    this.authService.logout();
    this.router.navigate(['login']);
  }

  scrollToTop() {
    $('html, body').animate({scrollTop: 0});
    return false;
  }
}
