import {Component, OnInit} from "@angular/core";
import {FormGroup, FormBuilder} from "@angular/forms";
import {ValidatorService, ErrorService, NotifyService, HttpService} from "../shared";

@Component({
  selector: 'kd-forgot',
  templateUrl: 'forgot.component.html'
})
export class ForgotComponent implements OnInit {
  forgotForm: FormGroup;
  forgotAttempt: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private validatorService: ValidatorService,
              private httpService: HttpService,
              private errorService: ErrorService,
              private notifyService: NotifyService) {
  }

  ngOnInit(): void {
    this.forgotForm = this.formBuilder.group({
      forgotEmail: ['', [this.validatorService.required, this.validatorService.isEmail]]
    });
  }

  forgot() {
    this.forgotAttempt = true;
    if (!this.forgotForm.valid) {
      return;
    }
    let forgotEmail = this.forgotForm.value.forgotEmail;
    this.httpService.post('/forgot', {forgotemail: forgotEmail})
      .subscribe(
        data => {
          if (data['success']) {
            this.notifyService.notify(forgotEmail, 'success', "An email has been sent with further instructions");
          }
          this.forgotAttempt = false;
          this.forgotForm.reset();
        },
        error => this.errorService.handleError(error)
      );
  }

  hasError(control: string): boolean {
    return !this.forgotForm.get(control).valid && this.forgotAttempt;
  }

}
