import {Component} from "@angular/core";

@Component({
  selector: 'kd-login-footer',
  template: `
    <div class="clearfix"></div>
    <br/>
    <div>
      <h2><i class="fa fa-group"></i> Kidorah</h2>
      <p>
        <i class="fa fa-copyright" aria-hidden="true"></i>
        2016 All Rights Reserved. Privacy and Terms
      </p>
    </div>
  `
})
export class LoginFooterComponent {
}
