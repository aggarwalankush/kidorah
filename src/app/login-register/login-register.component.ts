import {Component, OnDestroy, OnInit} from "@angular/core";

@Component({
  template: `
    <div>
      <a class="hiddenanchor" id="register"></a>
      <a class="hiddenanchor" id="login"></a>
      <a class="hiddenanchor" id="forgot"></a>
      <div class="login_wrapper">
        <kd-login></kd-login>
        <kd-forgot></kd-forgot>
        <kd-register></kd-register>
      </div>
    </div>
`
})
export class LoginRegisterComponent implements OnDestroy, OnInit {

  ngOnInit(): void {
    $('body').addClass('login');
  }

  ngOnDestroy(): void {
    $('body').removeClass('login');
  }
}
