import {NgModule} from "@angular/core";
import {SharedModule} from "../shared";
import {LoginComponent} from "./login.component";
import {LoginFooterComponent} from "./login-footer.component";
import {RegisterComponent} from "./register.component";
import {ForgotComponent} from "./forgot.component";
import {loginRouting} from "./login-register.routing";
import {LoginRegisterComponent} from "./login-register.component";

@NgModule({
  imports: [SharedModule, loginRouting],
  declarations: [
    LoginRegisterComponent,
    LoginComponent,
    LoginFooterComponent,
    RegisterComponent,
    ForgotComponent
  ]
})
export class LoginRegisterModule {
}
