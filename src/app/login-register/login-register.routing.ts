import {Routes, RouterModule} from "@angular/router";
import {LoginRegisterComponent} from "./login-register.component";
import {LoginGuardService} from "../shared";

export const loginRoutes: Routes = [
  {
    path: 'login',
    component: LoginRegisterComponent,
    canActivate: [LoginGuardService]
  }
];

export const loginRouting = RouterModule.forChild(loginRoutes);
