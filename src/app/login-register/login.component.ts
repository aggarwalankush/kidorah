import {Component, OnInit} from "@angular/core";
import {Router, NavigationExtras} from "@angular/router";
import {FormGroup, FormBuilder} from "@angular/forms";
import {ValidatorService, User, ErrorService, AuthService, HttpService} from "../shared";

@Component({
  selector: 'kd-login',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginAttempt: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private httpService: HttpService,
              private validatorService: ValidatorService,
              private errorService: ErrorService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      loginUsername: ['', [this.validatorService.required]],
      loginPassword: ['', [this.validatorService.required]]
    });
  }

  login() {
    this.loginAttempt = true;
    if (!this.loginForm.valid) {
      return;
    }
    const user = new User(
      this.loginForm.value.loginUsername,
      this.loginForm.value.loginPassword
    );
    this.httpService.post('/login', user)
      .subscribe(
        data => {
          this.loginAttempt = false;
          this.loginForm.reset();

          localStorage.setItem('token', data.token);
          localStorage.setItem('userId', data['userId']);
          localStorage.setItem('role', data['role']);
          localStorage.setItem('name', data['name']);
          this.redirectIfLogin();
        },
        error => this.errorService.handleError(error)
      );
  }

  redirectIfLogin() {
    if (this.authService.isLoggedIn()) {
      let navigationExtras: NavigationExtras = {
        preserveQueryParams: true,
        preserveFragment: false
      };
      this.router.navigate([this.authService.redirectUrl], navigationExtras);
    }
  }

  hasError(control: string) {
    return !this.loginForm.get(control).valid && this.loginAttempt;
  }
}
