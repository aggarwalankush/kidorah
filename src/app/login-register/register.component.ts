import {Component, OnInit} from "@angular/core";
import {FormGroup, FormBuilder, Validators, FormArray, FormControl} from "@angular/forms";
import {ValidatorService, ErrorService, NotifyService, Team, HttpService} from "../shared";

@Component({
  selector: 'kd-register',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  registerAttempt: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private httpService: HttpService,
              private validatorService: ValidatorService,
              private errorService: ErrorService,
              private notifyService: NotifyService) {
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      teamName: [
        '',
        [this.validatorService.required, Validators.minLength(3)],
        this.validatorService.teamNameValidator.bind(this.validatorService)
      ],
      teamAdmin: ['', [this.validatorService.required, this.validatorService.isEmail]],
      teamMembers: new FormArray([])
    });
  }


  addTeamMember(inputEmail: string) {
    (<FormArray>this.registerForm.controls['teamMembers']).push(
      new FormControl(inputEmail, [this.validatorService.required, this.validatorService.isEmail])
    );
  }

  removeTeamMember(index: number) {
    (<FormArray>this.registerForm.controls['teamMembers']).removeAt(index);
  }

  registerTeam() {
    this.registerAttempt = true;
    if (!this.registerForm.valid) {
      return;
    }
    const team = new Team(
      this.registerForm.value.teamName,
      this.registerForm.value.teamAdmin,
      this.registerForm.value.teamMembers
    );
    this.httpService.post('/team', team)
      .subscribe(
        data => {
          if (data['success']) {
            this.notifyService.notify(
              'Team Request Recieved',
              'success',
              team.teamname + " will be reviewed shortly"
            );
          }
          this.registerAttempt = false;
          this.registerForm.reset();
        },
        error => this.errorService.handleError(error)
      );
  }

  hasError(control: string|FormControl): boolean {
    if (typeof control === 'string') {
      return !this.registerForm.get(control).valid && this.registerAttempt;
    }
    if (control instanceof FormControl) {
      return !control.valid && this.registerAttempt;
    }
  }

}
