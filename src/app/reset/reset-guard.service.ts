import {Injectable} from "@angular/core";
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {ErrorService, AuthService, HttpService} from "../shared";
import "rxjs/add/observable/of";

@Injectable()
export class ResetGuardService implements CanActivate {

  constructor(private authService: AuthService,
              private httpService: HttpService,
              private errorService: ErrorService,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['login']);
      return Observable.of(false);
    }
    let token = route.params['token'];
    return new Observable((obs: any) => {
      return this.httpService.get('/reset/' + token)
        .subscribe(
          data=> {
            obs.next(data['success'] === true);
            obs.complete();
          },
          (err)=> {
            this.errorService.handleError(err);
            this.router.navigate(['login']);
            obs.next(false);
            obs.complete();
          }
        );
    });
  }
}
