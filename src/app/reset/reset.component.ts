import {Component, OnInit, OnDestroy} from "@angular/core";
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ErrorService, AuthService, HttpService, ValidatorService} from "../shared";

@Component({
  templateUrl: 'reset.component.html'
})
export class ResetComponent implements OnInit, OnDestroy {
  resetForm: FormGroup;
  resetAttempt: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private httpService: HttpService,
              private validatorService: ValidatorService,
              private errorService: ErrorService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnDestroy(): void {
    $('body').removeClass('login');
  }

  ngOnInit(): void {
    $('body').addClass('login');
    this.resetForm = this.formBuilder.group({
      newPassword: ['', [this.validatorService.required, Validators.minLength(8)]],
      confirmPassword: ['', [this.validatorService.required, this.isEqualPassword.bind(this)]]
    });
  }

  hasError(control: string) {
    return !this.resetForm.get(control).valid && this.resetAttempt;
  }

  resetPassword() {
    this.resetAttempt = true;
    if (!this.resetForm.valid) {
      return;
    }
    let token = this.route.snapshot.params['token'];
    let password = this.resetForm.value.newPassword.trim();
    this.httpService.post('/reset/' + token, {password: password})
      .subscribe(
        data => {
          this.resetAttempt = false;
          this.resetForm.reset();
          localStorage.setItem('token', data['token']);
          localStorage.setItem('userId', data['userId']);
          localStorage.setItem('role', data['role']);
          localStorage.setItem('name', data['name']);
          if (this.authService.isLoggedIn()) {
            this.router.navigate(['']);
          }
        },
        error => this.errorService.handleError(error)
      );
  }

  isEqualPassword(control: FormControl): {[s: string]: boolean} {
    if (!this.resetForm) {
      return {passwordsNotMatch: true};
    }
    if (control.value !== this.resetForm.controls['newPassword'].value) {
      return {passwordsNotMatch: true};
    }
  }
}
