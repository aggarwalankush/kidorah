import {NgModule} from "@angular/core";
import {SharedModule} from "../shared";
import {resetRouting} from "./reset.routing";
import {ResetGuardService} from "./reset-guard.service";
import {ResetComponent} from "./reset.component";

@NgModule({
  imports: [SharedModule, resetRouting],
  declarations: [ResetComponent],
  providers: [ResetGuardService]
})
export default class ResetModule {
}
