import {RouterModule, Routes} from "@angular/router";
import {ResetComponent} from "./reset.component";
import {ResetGuardService} from "./reset-guard.service";

const resetRoutes: Routes = [
  {
    path: '',
    component: ResetComponent,
    canActivate: [ResetGuardService]
  }
];

export const resetRouting = RouterModule.forChild(resetRoutes);
