import {Injectable} from "@angular/core";
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "./auth.service";

/**
 * Check if user is logged in
 */
@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.isLoggedIn()) {
      return true;
    }

    // Store the attempted URL for redirecting
    this.authService.redirectUrl = state.url;
    // Navigate to the login page with extras
    this.router.navigate(['login']);
    return false;
  }
}

/**
 * Check if user is SiteAdmin
 */
@Injectable()
export class SiteAdminGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.isSiteAdmin()) {
      return true;
    }
    this.authService.redirectUrl = state.url;
    if (!this.authService.isSiteAdmin()) {
      this.router.navigate(['']);
    } else {
      this.router.navigate(['login']);
    }
    return false;
  }
}

/**
 * Check if user is Admin or Member
 */
@Injectable()
export class UserGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.authService.isSiteAdmin()) {
      return true;
    }
    this.authService.redirectUrl = state.url;
    if (this.authService.isSiteAdmin()) {
      this.router.navigate(['sa']);
    } else {
      this.router.navigate(['login']);
    }
    return false;
  }
}

/**
 * Check if user is Admin or Member
 */
@Injectable()
export class LoginGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.authService.isLoggedIn()) {
      return true;
    }
    if (this.authService.isSiteAdmin()) {
      this.router.navigate(['sa']);
    } else {
      this.router.navigate(['']);
    }
    return false;
  }
}
