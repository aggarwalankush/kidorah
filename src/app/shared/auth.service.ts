import {Injectable} from "@angular/core";

@Injectable()
export class AuthService {
  redirectUrl: string = '';

  logout() {
    localStorage.clear();
  }

  isLoggedIn() {
    return localStorage.getItem('token') !== null;
  }

  isSiteAdmin() {
    return localStorage.getItem('role') === 'siteadmin';
  }

  isAdmin() {
    return localStorage.getItem('role') === 'admin';
  }

  getUserName() {
    return localStorage.getItem('name');
  }

}
