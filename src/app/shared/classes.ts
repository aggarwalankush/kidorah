export class User {
  constructor(public username: string,
              public password: string,
              public role?: string,
              public name?: string,
              public email?: string) {
  }
}

export class Team {
  constructor(public teamname: string,
              public teamadmin: string,
              public teammember: Array<string>) {
  }
}
