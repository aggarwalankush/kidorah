import {Injectable} from "@angular/core";
import {NotifyService} from "./notify.service";

@Injectable()
export class ErrorService {

  constructor(private notifyService: NotifyService) {
  }

  handleError(error: string) {
    this.notifyService.notify(error || 'An Error Occurred', 'error');
  }

}
