import {Injectable} from "@angular/core";
import {Http, Headers, URLSearchParams} from "@angular/http";
import * as _ from "lodash";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/operator/finally";
import "rxjs/add/operator/catch";
import {NotifyService} from "./notify.service";
import {environment} from "../index";

@Injectable()
export class HttpService {
  constructor(private http: Http, private notifyService: NotifyService) {
  }

  get(url: string, params?: URLSearchParams): Observable<any> {
    this.notifyService.progressStart();
    const headers = new Headers({
      'x-access-token': localStorage.getItem('token') || ''
    });
    let queryParams = params;
    if (_.isPlainObject(params)) {
      queryParams = new URLSearchParams();
      _.forEach(params, (value, key)=> {
        if (value) {
          queryParams.set(key, value);
        }
      });
    }
    return this.http.get(environment.serverHost + url, {headers: headers, search: queryParams})
      .map(response => response.json())
      .finally(()=> {
        this.notifyService.progressDone()
      })
      .catch(error => Observable.throw(error.json().error));
  }

  post(url: string, dataObj: any): Observable<any> {
    this.notifyService.progressStart();
    const body = JSON.stringify(dataObj);
    const headers = new Headers({
      'Content-Type': 'application/json',
      'x-access-token': localStorage.getItem('token') || ''
    });
    return this.http.post(environment.serverHost + url, body, {headers: headers})
      .map(response => response.json())
      .finally(()=> {
        this.notifyService.progressDone()
      })
      .catch(error => Observable.throw(error.json().error));
  }

  patch(url: string, dataObj: any): Observable<any> {
    this.notifyService.progressStart();
    const body = JSON.stringify(dataObj);
    const headers = new Headers({
      'Content-Type': 'application/json',
      'x-access-token': localStorage.getItem('token') || ''
    });
    return this.http.patch(environment.serverHost + url, body, {headers: headers})
      .map(response => response.json())
      .finally(()=> {
        this.notifyService.progressDone()
      })
      .catch(error => Observable.throw(error.json().error));
  }
}
