import {Injectable} from "@angular/core";
declare var PNotify: any;
var NProgress = require('nprogress');
PNotify.prototype.options.styling = "bootstrap3";

@Injectable()
export class NotifyService {

  notify(title, type?, text?, delay?) {
    PNotify.removeAll();
    new PNotify({
      title: title,
      type: type || 'info',
      text: text || '',
      delay: delay || 5000
    });
  }

  progressStart() {
    NProgress.start();
    NProgress.inc(0.4);
  }

  progressDone() {
    NProgress.done();
  }
}
