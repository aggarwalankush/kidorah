import {NgModule, ModuleWithProviders} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AuthService} from "./auth.service";
import {AuthGuardService, SiteAdminGuardService, UserGuardService, LoginGuardService} from "./auth-guard.service";
import {ErrorService} from "./error.service";
import {NotifyService} from "./notify.service";
import {ValidatorService} from "./validator.service";
import {HttpService} from "./http.service";
import {TableService} from "./table.service";

@NgModule({
  imports: [CommonModule, HttpModule],
  exports: [CommonModule, HttpModule, ReactiveFormsModule]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        AuthService,
        AuthGuardService,
        SiteAdminGuardService,
        UserGuardService,
        LoginGuardService,
        HttpService,
        ErrorService,
        NotifyService,
        ValidatorService,
        TableService
      ]
    };
  }
}
