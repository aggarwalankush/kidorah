import {Injectable} from "@angular/core";
import * as _ from "lodash";

@Injectable()
export class TableService {

  defaultTableOptions = {
    pageSize: 5,
    pageList: [5, 10, 20, 50, 100, 200],
    sidePagination: 'server',
    pagination: true,
    search: true,
    showRefresh: true
  };

  initTable(tableId: string, options) {
    $('#' + tableId).bootstrapTable(_.merge({}, this.defaultTableOptions, options));
  }

  refreshTable(tableId: string|Array<string>) {
    if (_.isArray(tableId)) {
      _.forEach(tableId, id => {
        $('#' + id).bootstrapTable('refresh');
      });
    } else if (typeof tableId === 'string') {
      $('#' + tableId).bootstrapTable('refresh');
    }
  }

  createEvent(eventName: string, eventType: string, callback) {
    _.set(window, eventName, {});
    _.set(window[eventName], eventType, callback);
  }

  removeEvent(eventName: string) {
    _.unset(window, eventName);
  }

  destroyTable(tableId: string|Array<string>) {
    if (_.isArray(tableId)) {
      _.forEach(tableId, id => {
        $('#' + id).bootstrapTable('destroy');
      });
    } else if (typeof tableId === 'string') {
      $('#' + tableId).bootstrapTable('destroy');
    }
  }
}
