import {Injectable} from "@angular/core";
import {FormControl} from "@angular/forms";
import {URLSearchParams} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import "rxjs/add/operator/map";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/do";
import "rxjs/add/operator/share";
import "rxjs/add/operator/take";
import "rxjs/add/observable/of";
import {HttpService} from "./http.service";

@Injectable()
export class ValidatorService {

  constructor(private httpService: HttpService) {
  }

  private validatorInput: Subject<string> = new Subject<string>();
  validatorChain: Observable<any> = this.validatorInput
    .debounceTime(400)
    .distinctUntilChanged()
    .switchMap((value: string) => {
      let params = new URLSearchParams();
      params.set('name', value);
      return this.httpService.get('/team', params);
    })
    .map(res=> res['success'] === true ? {'teamTaken': true} : null)
    .catch(err=>Observable.of(null))
    .share()
    .take(1);


  teamNameValidator(control: FormControl): Observable<any> {
    if (!control.value) {
      return Observable.of({'teamTaken': true});
    }
    setTimeout(()=>this.validatorInput.next(control.value.trim()), 0);
    return this.validatorChain;
  }

  isEmail(control: FormControl): {[s: string]: boolean} {
    if (!control.value) {
      return {'invalidEmail': true};
    }
    let emailPattern: string = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
    return !control.value.match(emailPattern) ? {'invalidEmail': true} : null;
  }

  required(control: FormControl): {[s: string]: boolean} {
    if (!control.value) {
      return {"required": true};
    }
    return control.value.trim() === '' ? {"required": true} : null;
  }

}
